package com.springboot.tutorial.springbootTutorial.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository stdRepository){
        return args -> {
            Student mariam = new Student("Mariam","mariam.gmail.com", LocalDate.of(2000, Month.JANUARY, 5));
            Student jamal = new Student("Jamal","jamal.gmail.com", LocalDate.of(2000, Month.JANUARY, 5));
            stdRepository.saveAll(List.of(mariam, jamal));
        };
    }
}
