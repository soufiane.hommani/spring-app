package com.springboot.tutorial.springbootTutorial.student;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/student")
public class StudentController {
    //private final StudentService stdService = reference à StudentService
    private final StudentService stdService;


    @Autowired
    public StudentController(StudentService stdService) {
        this.stdService = stdService;
    }

    @GetMapping
    public List<Student> getStudents(){
        return stdService.getStudents();
    }


    //@RequestBody = map le json student de la requete post en objet student
    @PostMapping
    public void registerNewStudent(@RequestBody Student student){
        stdService.addNewStudents(student);
    }

    @DeleteMapping(path = "{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long studentId){

        stdService.deleteStudent(studentId);

    }

    @PutMapping(path = "{studentId}")
    public void updateStudent(
            @PathVariable("studentId") Long studentId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email){

        stdService.updateStudent(studentId, name, email);

    }



}
