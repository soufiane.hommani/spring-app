package com.springboot.tutorial.springbootTutorial.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {


    //SELECT * FROM ...JBQL and not SQL :
    //@Query("SELECT s FROM Student s WHERE s.email = ?1")
    Optional<Student> findStudentByEmail(String email);



}
