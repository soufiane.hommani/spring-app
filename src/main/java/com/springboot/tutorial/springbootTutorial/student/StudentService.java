package com.springboot.tutorial.springbootTutorial.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository stdRepository;

    @Autowired
    public StudentService(StudentRepository stdRepository){
        this.stdRepository = stdRepository;
    }
    public List<Student> getStudents() {
        return stdRepository.findAll();
    }
    public void addNewStudents(Student student) {
        Optional<Student> studentOptional = stdRepository.findStudentByEmail(student.getEmail());

        if(studentOptional.isPresent()){
            throw new IllegalStateException("email taken");
        }
        stdRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        boolean exists = stdRepository.existsById(studentId);
        if(!exists){
            throw new IllegalStateException("student with id" + studentId + "does not exist");
        }
        stdRepository.deleteById(studentId);
    }

    @Transactional
    public void updateStudent(Long studentId, String name, String email) {
        boolean exists = stdRepository.existsById(studentId);
        if(!exists){
            throw new IllegalStateException("student with id" + studentId + "does not exist");
        }

        Student studentExists = stdRepository.findById(studentId).orElseThrow(
                ()->new IllegalStateException("student with id" + studentId + "does not exist")
        );

        System.out.println(name + " " + email);
        studentExists.setName(name);
        studentExists.setEmail(email);


    }
}
